package com.example.chatapp.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.chatapp.Interfaces.getclick2
import com.example.chatapp.models.MessegesModel
import com.example.chatapp.models.UserModel
import com.example.chatapp.R
import com.example.chatapp.databinding.LastestMessegesBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.lastest_messeges.view.*

class MvvmAdapter(private val mylis:MutableList<MessegesModel>,val getclick: getclick2) : RecyclerView.Adapter<MvvmAdapter.holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MvvmAdapter.holder {
        val binding: LastestMessegesBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.lastest_messeges,
            parent,
            false
        )
        return holder(binding)
    }

    override fun getItemCount(): Int=mylis.size

    override fun onBindViewHolder(holder: MvvmAdapter.holder, position: Int) {

        holder.onbind()
    }
    inner class holder(val binding:LastestMessegesBinding) : RecyclerView.ViewHolder(binding.root) {
        private   var usermodel: MessegesModel?=null
        private lateinit var  senderusername:String
        lateinit var model: UserModel
        fun onbind(){

            usermodel=mylis[adapterPosition]
            binding.messegemodels=usermodel
            val userid= FirebaseAuth.getInstance().uid
            if(usermodel!!.senderid==userid){
                senderusername= usermodel!!.receverid
                binding.sender="You: "
            }
            else{
                binding.sender="messege: "
                senderusername= usermodel!!.senderid!!
            }
            val reference= FirebaseDatabase.getInstance().getReference("/users/$senderusername")
            reference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(p0: DataSnapshot) {
                    model= p0.getValue(UserModel::class.java)!!
                    binding.usersmodels=model
                }
            })
            itemView.mainitem2.setOnClickListener(){
                getclick.getsenderuid(model)
            }
        }
    }
}