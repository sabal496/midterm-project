package com.example.chatapp.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.chatapp.models.UserModel
import com.example.chatapp.R
import com.example.chatapp.Interfaces.getclick

import kotlinx.android.synthetic.main.recycler_users.view.*

class Adapter(val mylis:MutableList<UserModel>, val getclick: getclick): RecyclerView.Adapter<Adapter.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_users,parent,false))
    }

    override fun getItemCount(): Int {
        return mylis.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
    }

    inner class holder(itemView: View): RecyclerView.ViewHolder(itemView){
        private  lateinit var usermodel: UserModel

        fun onbind(){
            usermodel=mylis[adapterPosition]
            itemView.username.text=usermodel.firstName
            Glide.with(itemView.context).load(usermodel.imageurl).into(itemView.userphoto)
            itemView.mainitem.setOnClickListener(){
                getclick.getPosition(adapterPosition)
            }
        }

    }
}