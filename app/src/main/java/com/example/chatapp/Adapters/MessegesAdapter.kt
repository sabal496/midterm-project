package com.example.chatapp.Adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater.*
import com.bumptech.glide.Glide
import com.example.chatapp.models.MessegesModel
import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.from_view.view.*
import kotlinx.android.synthetic.main.to_view.view.*

class MessegesAdapter(val mylis:MutableList<MessegesModel>, val curentlink: String, val userlink:String?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object{
        const val MAIN_ITEM=1
        const val SECOND_ITEM=2

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if(viewType== MAIN_ITEM){
            return  mainviewholder(from(parent.context).inflate(R.layout.to_view,parent,false))
        }
        return secondviewholder(from(parent.context).inflate(R.layout.from_view,parent,false))
    }

    override fun getItemCount(): Int {
        return mylis.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if(holder is mainviewholder) holder.onbind()
        else if(holder is secondviewholder) holder.onbind()

    }


    inner class mainviewholder(itemView: View): RecyclerView.ViewHolder(itemView) {
       lateinit var model: MessegesModel

        fun onbind(){
            model=mylis[adapterPosition]
            itemView.mymessege.text=model.messege
            Glide.with(itemView.context).load(curentlink).into( itemView.myfoto)

        }

    }

    inner class secondviewholder(itemView: View): RecyclerView.ViewHolder(itemView){
        lateinit var model: MessegesModel
        fun onbind(){
            model = mylis[adapterPosition]
            itemView.messagefrom.text=model.messege
           Glide.with(itemView.context).load(userlink).into(itemView.userphotomessege)
        }

    }

    override fun getItemViewType(position: Int): Int {
        val userid= FirebaseAuth.getInstance().uid

        if(mylis[position].senderid==userid) return MAIN_ITEM
        return SECOND_ITEM
    }
}
