package com.example.chatapp.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.chatapp.Activities.LastestMessegesActivity
import com.example.chatapp.Activities.RegisterActivity
import com.example.chatapp.App

import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_login.view.*

class loginFragment : BaseFragment() {


    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
     }

    override fun getresource(): Int =R.layout.fragment_login

    private  fun init(){

        val useemail= itemView!!.loginemail.text
        val userpassword= itemView!!.loginpassword.text
        itemView!!.loginbtn.setOnClickListener(){
            if(useemail.isNotEmpty() && userpassword.isNotEmpty()){
                loginuser(useemail.toString(),userpassword.toString())
            }
            else {
                Toast.makeText(App.context,"all fields are requared!!", Toast.LENGTH_SHORT).show()
            }
        }

        itemView!!.register.setOnClickListener(){
            val intent= Intent(App.context, RegisterActivity::class.java)
            startActivity(intent)
            if (activity!=null) activity?.finish()
        }
    }

    private fun loginuser(email:String,password:String){
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password).addOnCompleteListener() {
            if(!it.isSuccessful)
                return@addOnCompleteListener
            else{
                val intent= Intent(App.context,
                    LastestMessegesActivity::class.java)
                intent.flags= Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }.addOnFailureListener() {
            Toast.makeText(App.context,it.message, Toast.LENGTH_SHORT).show()
        }
    }

}
