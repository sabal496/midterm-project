package com.example.chatapp.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.chatapp.Activities.LastestMessegesActivity
import com.example.chatapp.Activities.LoginActivity
import com.example.chatapp.Activities.RegisterActivity
import com.example.chatapp.App

import com.example.chatapp.R
import com.example.chatapp.models.UserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import java.util.*
import kotlinx.android.synthetic.main.fragment_register.view.*

class RegisterFragment : BaseFragment() {
    companion object{
        val REQUEST_CODE=1
    }
    var imageuri: Uri?=null
    lateinit var useremail: Editable
    lateinit var userpassword: Editable
    lateinit var userfirstname: Editable
    lateinit var userlastname: Editable
    var imageclicked=false

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {

        init()
     }

    override fun getresource(): Int =R.layout.fragment_register

    private  fun init(){
        itemView!!.signin.setOnClickListener(){
            val intent= Intent(App.context,
                LoginActivity::class.java)
            startActivity(intent)
        }
        useremail=itemView!!.email.text
        userpassword=itemView!!.password.text
        userfirstname=itemView!!.firstname.text
        userlastname=itemView!!.lastname.text

        itemView!!.registerbtn.setOnClickListener(){
            if(useremail.isNotEmpty() && userpassword.isNotEmpty() && userfirstname.isNotEmpty() && userlastname.isNotEmpty() && imageclicked){
                RegisterUSer(useremail.toString(),userpassword.toString())
            }
            else{
                Toast.makeText(context,"all fields are requaired!!", Toast.LENGTH_SHORT).show()
            }
        }

        itemView!!.photobutton.setOnClickListener(){
            imageclicked=true
            val intent= Intent(Intent.ACTION_PICK)
            intent.type="image/*"
            startActivityForResult(intent,
                REQUEST_CODE
            )
        }
    }

    //register user
    private fun RegisterUSer ( email:String, password:String){
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener(){
                if(!it.isSuccessful)return@addOnCompleteListener
                Toast.makeText(App.context,it.result?.user?.uid.toString(), Toast.LENGTH_SHORT).show()
                uploadImage()
            }.addOnFailureListener(){
                Toast.makeText(App.context,it.message, Toast.LENGTH_SHORT).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode== REQUEST_CODE && resultCode== Activity.RESULT_OK && data!=null){
            if(data==null) Toast.makeText(context,"please choose your image", Toast.LENGTH_SHORT).show()
            else{
                imageuri=data?.data
                itemView!!.photobutton.setImageURI(imageuri)
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    //upload selected image to firebase store
    private fun uploadImage(){
        val randomname:String= UUID.randomUUID().toString()
        val ref= FirebaseStorage.getInstance().getReference("images/$randomname")
        ref.putFile(imageuri!!).addOnSuccessListener {
            Toast.makeText(context,"image was uploaded successfully", Toast.LENGTH_SHORT).show()
            var downloadurl:String=""
            //get image url
            ref.downloadUrl.addOnSuccessListener {
                downloadurl=it.toString()
                uploaddata(downloadurl)
            }
        }.addOnFailureListener(){
            Toast.makeText(context,it.message, Toast.LENGTH_SHORT).show()
        }
    }

    //uploading  data to firebase database
    private fun uploaddata(downloadurl:String){
        val userid= FirebaseAuth.getInstance().uid
        val ref= FirebaseDatabase.getInstance().getReference("/users/${userid}")
        val user= UserModel(
            userid,
            userfirstname.toString(),
            userlastname.toString(),
            useremail.toString(),
            downloadurl
        )
        ref.setValue(user).addOnSuccessListener {
            Toast.makeText(context,"data was uploaded succseccsfully", Toast.LENGTH_SHORT).show()
            val intent= Intent(context,
                LastestMessegesActivity::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }.addOnFailureListener(){
            Toast.makeText(context,it.message, Toast.LENGTH_SHORT).show()
        }
    }

}
