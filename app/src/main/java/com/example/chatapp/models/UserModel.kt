package com.example.chatapp.models

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class UserModel(var uid:String?="", var firstName:String?="", val latName:String?="", val email:String?="", val imageurl:String?=""):Parcelable {


}