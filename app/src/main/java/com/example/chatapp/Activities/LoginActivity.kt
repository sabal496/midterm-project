package com.example.chatapp.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.chatapp.R
import com.example.chatapp.fragments.loginFragment
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        addfragment()
    }


    private fun addfragment(){
        val transaction = supportFragmentManager.beginTransaction()
        val fragment=loginFragment()
        transaction.add(R.id.loginactivity,fragment,"loginfragment")
        transaction.commit()

    }
}

