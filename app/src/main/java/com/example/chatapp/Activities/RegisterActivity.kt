package com.example.chatapp.Activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Toast
import com.example.chatapp.models.UserModel
import com.example.chatapp.R
import com.example.chatapp.fragments.RegisterFragment
import com.example.chatapp.fragments.loginFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class RegisterActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addfragment()
    }


    private fun addfragment(){
        val transaction = supportFragmentManager.beginTransaction()
        val fragment= RegisterFragment()
        transaction.add(R.id.registeracitivity,fragment,"loginfragment")
        transaction.commit()

    }


}
