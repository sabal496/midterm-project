package com.example.chatapp.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        init()
    }
    private fun init(){
        Handler().postDelayed({startnewactivity()},3000)
    }
    private fun startnewactivity(){
        lateinit var intent:Intent
        if(FirebaseAuth.getInstance().uid==null){
              intent=Intent(this,LoginActivity::class.java)
        }
        else {
              intent=Intent(this,LastestMessegesActivity::class.java)
        }
        startActivity(intent)
        finish()

    }


}
