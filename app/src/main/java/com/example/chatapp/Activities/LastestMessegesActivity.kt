package com.example.chatapp.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatapp.Adapters.MvvmAdapter
import com.example.chatapp.Interfaces.getclick2
import com.example.chatapp.models.MessegesModel
import com.example.chatapp.models.UserModel
import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_lastest_messeges.*

class LastestMessegesActivity : AppCompatActivity() {

    val lastmessegemap= mutableMapOf<String?,MessegesModel?>()
    var list= mutableListOf<MessegesModel>()
    lateinit var userModel: UserModel
    lateinit var mvvmdapter: MvvmAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_lastest_messeges)
        getlastmesseges()
    }

    private fun  addadapter(map :MutableMap<String?,MessegesModel?>){
        lastmessegesrecycle.layoutManager=LinearLayoutManager(this)
        list.clear()
        map?.values?.forEach(){
            if (it != null) {
                list.add(it)
            }
        }

        mvvmdapter= MvvmAdapter(list,object :getclick2{
            override fun getsenderuid(uid: UserModel) {
                userModel=uid
                getuserimglink(userModel)

            }
        })
        lastmessegesrecycle.adapter=mvvmdapter
    }

    private fun getlastmesseges(){
        val userid= FirebaseAuth.getInstance().uid
        val ref=FirebaseDatabase.getInstance().getReference("/lasteste_messeges/$userid")
        ref.addChildEventListener(object :ChildEventListener{
            override fun onCancelled(p0: DatabaseError) {
             }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
             }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val lastmessege=p0.getValue(MessegesModel::class.java)
               lastmessegemap[p0.key]=lastmessege
                addadapter(lastmessegemap)
             }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val lastmessege=p0.getValue(MessegesModel::class.java)
               lastmessegemap[p0.key]=lastmessege
                addadapter(lastmessegemap)
             }

            override fun onChildRemoved(p0: DataSnapshot) {
             }
        })
    }

    protected fun getuserimglink(userModel: UserModel?){
        lateinit var r:UserModel
        lateinit var userimg:String
        val user=FirebaseAuth.getInstance().uid
        val ref=FirebaseDatabase.getInstance().getReference("/users/$user")
        ref.addListenerForSingleValueEvent(object:ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }
            override fun onDataChange(p0: DataSnapshot) {
                val r= p0.getValue(UserModel::class.java)!!
                userimg=r.imageurl.toString()

                if(userModel!=null){
                    val intent = Intent(
                        this@LastestMessegesActivity,
                        SendmMessegeActivity::class.java
                    )
                    intent.putExtra("usermodel", userModel)
                    intent.putExtra("userslink",userimg)
                    startActivity(intent)
                }
                else{
                    val intent=Intent(this@LastestMessegesActivity, ComposeMessegeActivity::class.java)
                    intent.putExtra("userslink",userimg)
                    startActivity(intent)
                }

            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.signout ->{
                FirebaseAuth.getInstance().signOut()
                val intent= Intent(this, LoginActivity::class.java)
                intent.flags= Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            R.id.newmessage ->{
                getuserimglink(null)


            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }
}
