package com.example.chatapp.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatapp.Adapters.MessegesAdapter
import com.example.chatapp.models.MessegesModel
import com.example.chatapp.models.UserModel
import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_sendm_messege.*

class SendmMessegeActivity : AppCompatActivity() {
    lateinit var adapter: MessegesAdapter
    var list = mutableListOf<MessegesModel>()
    lateinit var model: UserModel
    lateinit var mylink:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sendm_messege)

        init()
    }
    private fun init(){
        mylink = intent.extras?.getString("userslink").toString()
        model=intent.getParcelableExtra<UserModel>("usermodel")
        val messege=messegetext.text
        supportActionBar?.title =model.firstName
        getmesseges()
        sendmessegebtn.setOnClickListener(){
            sendmessege(messege.toString())
        }


    }

    private  fun sendmessege(messege:String){
        val userid= FirebaseAuth.getInstance().uid
        val ref= FirebaseDatabase.getInstance().getReference("messeges/$userid/${model.uid}").push()
        val ref2= FirebaseDatabase.getInstance().getReference("messeges/${model.uid}/$userid").push()
        val ref3=FirebaseDatabase.getInstance().getReference("lasteste_messeges/$userid/${model.uid}")
        val ref4= FirebaseDatabase.getInstance().getReference("lasteste_messeges/${model.uid}/$userid")
        val messege= MessegesModel(
            messege,
            userid,
            model.uid!!,
            System.currentTimeMillis() / 1000
        )
        ref.setValue(messege).addOnSuccessListener {
            Toast.makeText(this,"Messege was sent",Toast.LENGTH_SHORT).show()
        }.addOnFailureListener(){
            Toast.makeText(this,it.message,Toast.LENGTH_SHORT).show()
        }
        ref2.setValue(messege)
        ref3.setValue(messege)
        ref4.setValue(messege)
        messegetext.text.clear()
    }

    private  fun getmesseges(){
        val userid= FirebaseAuth.getInstance().uid
        val ref=FirebaseDatabase.getInstance().getReference("messeges/$userid/${model.uid}")
        ref.addChildEventListener(object :ChildEventListener{
            override fun onCancelled(p0: DatabaseError) {
             }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
             }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
             }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val messege=p0.getValue(MessegesModel::class.java)
                if (messege != null) {
                    list.add(messege)
                    adapter.notifyItemInserted(list.size-1)
                    messegerecyclerview.scrollToPosition(list.size-1
                    )
                }
                if (messege != null) {
                    d("messege","++++++++++++++++++++++${messege.messege}+++++++++++++++++++++++++++")
                }
             }

            override fun onChildRemoved(p0: DataSnapshot) {
             }
        })
        adapter= MessegesAdapter(list,mylink,model.imageurl)
        messegerecyclerview.layoutManager=LinearLayoutManager(this)
        messegerecyclerview.adapter=adapter
    }
}
