package com.example.chatapp.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatapp.Adapters.Adapter
import com.example.chatapp.Interfaces.getclick
import com.example.chatapp.models.UserModel
import com.example.chatapp.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_compose_messege.*

class ComposeMessegeActivity : AppCompatActivity() {

    lateinit var adapter: Adapter
    val list= mutableListOf<UserModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compose_messege)
        supportActionBar?.title ="select user"

        getusers()
    }

    private fun getusers(){
        val userslink=intent.extras?.getString("userslink")
        val ref=FirebaseDatabase.getInstance().getReference("/users")
        ref.addListenerForSingleValueEvent(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
             }
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach(){
                    val user=it.getValue(UserModel::class.java)
                    if (user != null) {
                        list.add(user)
                     }
                }
                recycler.layoutManager=LinearLayoutManager(this@ComposeMessegeActivity)
                adapter=
                    Adapter(
                        list,
                        object : getclick {
                            override fun getPosition(position: Int) {
                                val intent = Intent(
                                    this@ComposeMessegeActivity,
                                    SendmMessegeActivity::class.java
                                )
                                val usermodel: UserModel = list[position]
                                intent.putExtra("usermodel", usermodel)
                                intent.putExtra("userslink",userslink)
                                startActivity(intent)
                                finish()
                            }
                        })
                recycler.adapter=adapter
             }
        })
    }
}
