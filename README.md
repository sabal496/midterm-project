ChatApp
ChatApp is a  messaging app inspired by messaging & social media apps. Written in Kotlin implements the Jetpack libraries, Firebase services. 
Used Tech
-kotlin
-firebase
-Authentication - Allows an app to securely save user data in the cloud.
-mvvm
-Data Binding - Declaratively bind observable data to UI elements.
-Lifecycles - Create a UI that automatically responds to lifecycle events.
-Navigation - Handle everything needed for in-app navigation.
-Glide - Load and cache images by URL.

Features
Firebase:
Authentication (Email)
Cloud Firestore
Cloud Messaging
Functions
Storage
Create user profile (Username)
Send text
 